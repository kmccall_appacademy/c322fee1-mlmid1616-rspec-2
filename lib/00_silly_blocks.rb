def reverser
  yield.split.map {|word| word.reverse}.join(" ")
end

def adder(number=1)
  number + 5
end

def repeater(count = 1)
  i = 0
  while i < count
    yield
    i+=1
  end
end
